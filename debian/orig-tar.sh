#!/bin/sh 

set -e

# called by uscan with '--upstream-version' <version> <file>
echo "version $2"
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
version="$(echo $2 | sed 's/^/0\.0~/')"
tarball=$3
TAR=${package}_${version}.orig.tar.gz
DIR=${package}-${version}.orig

unzip $tarball -d $DIR

GZIP=--best tar --numeric --group 0 --owner 0 --anchored \
   -X debian/orig-tar.excludes -c -v -z -f $TAR $DIR

rm -rf $tarball $DIR
